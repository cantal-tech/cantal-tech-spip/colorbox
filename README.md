# COLORBOX pour SPIP

Mise en paquet de la modalbox [colorbox](http://www.jacklmoore.com/colorbox/) pour SPIP.
Nécessite l'API Mediabox v2 pour SPIP disponible [ici](https://framagit.org/cantal-tech/mediabox).
