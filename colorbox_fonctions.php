<?php

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

/**
 * Tableau de la configuration du plugin
 * 
 * @return array       
**/
function colorbox_config() {

	$config = array(
		'nom' => 'colorbox',
		'options' => array(
			// valeurs par defaut, 
			'transition' => 'elastic',
			'maxWidth' => '1200px',
			'maxHeight' => '90%',
			'minWidth' => '300px',
			'minHeight' => '200px',
			'speed' => '200',
			'slideshow_speed' => '2500',
			'splash_width' => '600px',
			'splash_height' => '90%'
		),
		'fichiers' => array(
			'core' => array(
				'fichier' => 'jquery.colorbox.js',
				'chemin' => 'lib/colorbox/',
				// 'hors_compresseur' => true
			),
			'mediabox' => array(
				'fichier' => 'colorbox.mediabox.js', 
				'chemin' => 'javascript/',
			),						
			// 'i18n' => array(
			// 	'fichier' => 	'jquery.colorbox-'. $GLOBALS['spip_lang'] . '.js',
			// 	'chemin' => 'lib/colorbox/i18n/',
			// 	// 'hors_compresseur' => true
			// ),
		),
	);
	return $config;
}
/**
 * Pipeline mediabox_config
 * 
 * @return array       
**/
function colorbox_mediabox_config($flux) {

	$conf = colorbox_config();
	//completer les options manquantes
	foreach ($conf['options'] as $key => $value) {
		if (!isset($flux[$key])) {
			$flux[$key] = $value;
		}
	}
	//ajouter featherlight à la liste des libs disponibles
	$flux['_libs']['colorbox'] = $conf['fichiers'];
	return $flux;
}